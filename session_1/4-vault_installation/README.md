# Vault Installation

## Overview

Vault comes with various pluggable components called secrets engines and authentication methods allowing you to integrate with external systems. The purpose of those components is to manage and protect your secrets in dynamic infrastructure (e.g. database credentials, passwords, API keys).

## Install Vault
1. Add the HashiCorp GPG key

```
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
```

2. Add the official HashiCorp Linux repository

```
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
```

3. Update and install

```
sudo apt-get update && sudo apt-get install vault
```

2. Verify the installation

```
vault
```

The output should be:

```
Usage: vault <command> [args]

Common commands:
    read        Read data and retrieves secrets
    write       Write data, configuration, and secrets

...
```

## Vault RAFT storage folder
```
sudo mkdir -p /vault/data
sudo chown -P vault:vault /vault/data
```

## Vault autocomplete install

```
vault -autocomplete-install
```

```
complete -C /usr/bin/vault vault
```

## Vault server configuration

Vault is configured using HCL files. The configuration file for Vault is relatively simple

1. Create a configuration file at /etc/vault.d/server.hcl

```
sudo touch /etc/vault.d/server.hcl
```

2. Add this configuration to the server.hcl configuration file

Copy this configuration:

```
# server.hcl
ui = true
disable_mlock = true
api_addr = "http://127.0.0.1:8200"
cluster_addr = "https://127.0.0.1:8201"

storage "raft" {
  path = "/vault/data"
  node_id = "raft_node_1"
}

# CONSUL - alternative
#storage "consul" {
#  address = "127.0.0.1:8500"
#  path    = "vault/"
#}

# HTTP listener
listener "tcp" {
  address = "0.0.0.0:8200"
  tls_disable = 1
}
```

Open text editor:

```
sudo vi /etc/vault.d/server.hcl
```

Paste the configuration and then :qw and hit ENTER to write and quit from the text editor

## Configure the vault systemd service

```
sudo touch /etc/systemd/system/vault.service
```

Copy this configuration:

```
# vault.service
[Unit]
Description="HashiCorp Vault - A tool for managing secrets"
Documentation=https://www.vaultproject.io/docs/
Requires=network-online.target
After=network-online.target
ConditionFileNotEmpty=/etc/vault.d/vault.hcl
StartLimitIntervalSec=60
StartLimitBurst=3

[Service]
User=vault
Group=vault
ProtectSystem=full
ProtectHome=read-only
PrivateTmp=yes
PrivateDevices=yes
SecureBits=keep-caps
AmbientCapabilities=CAP_IPC_LOCK
Capabilities=CAP_IPC_LOCK+ep
CapabilityBoundingSet=CAP_SYSLOG CAP_IPC_LOCK
NoNewPrivileges=yes
ExecStart=/usr/bin/vault server -config=/etc/vault.d/server.hcl
ExecReload=/bin/kill --signal HUP $MAINPID
KillMode=process
KillSignal=SIGINT
Restart=on-failure
RestartSec=5
TimeoutStopSec=30
StartLimitInterval=60
StartLimitIntervalSec=60
StartLimitBurst=3
LimitNOFILE=65536
LimitMEMLOCK=infinity

[Install]
WantedBy=multi-user.target
```

Open text editor:

```
sudo vi /etc/systemd/system/vault.service
```

Paste the configuration and then :qw and hit ENTER to write and quit from the text editor

## Start the Vault service

Enable and start Vault using the systemctl command responsible for controlling systemd managed services

```
sudo systemctl start vault
```

Check the status of the vault service using systemctl

```
sudo systemctl status vault
```

The output should be:

```
● vault.service - "HashiCorp Vault - A tool for managing secrets"
     Loaded: loaded (/etc/systemd/system/vault.service; disabled; vendor preset: enabled)
     Active: active (running) since Wed 2020-12-09 14:04:40 UTC; 8s ago
       Docs: https://www.vaultproject.io/docs/
   Main PID: 17547 (vault)
      Tasks: 7 (limit: 4642)
     Memory: 13.7M
     CGroup: /system.slice/vault.service
             └─17547 /usr/bin/vault server -config=/etc/vault.d/server.hcl

Dec 09 14:04:40 ip-10-1-101-114 vault[17547]:               Go Version: go1.15.4
Dec 09 14:04:40 ip-10-1-101-114 vault[17547]:               Listener 1: tcp (addr: "0.0.0.0:8200", cluster address: "0.0.0.0:8201", max_request_duration: "1m30s", max_request_size: "33554432", tls: "disabled")
Dec 09 14:04:40 ip-10-1-101-114 vault[17547]:                Log Level: info
Dec 09 14:04:40 ip-10-1-101-114 vault[17547]:                    Mlock: supported: true, enabled: false
Dec 09 14:04:40 ip-10-1-101-114 vault[17547]:            Recovery Mode: false
Dec 09 14:04:40 ip-10-1-101-114 vault[17547]:                  Storage: consul (HA available)
Dec 09 14:04:40 ip-10-1-101-114 vault[17547]:                  Version: Vault v1.6.0
Dec 09 14:04:40 ip-10-1-101-114 vault[17547]:              Version Sha: 7ce0bd9691998e0443bc77e98b1e2a4ab1e965d4
Dec 09 14:04:40 ip-10-1-101-114 vault[17547]: ==> Vault server started! Log data will stream in below:
Dec 09 14:04:40 ip-10-1-101-114 vault[17547]: 2020-12-09T14:04:40.678Z [INFO]  proxy environment: http_proxy= https_proxy= no_proxy=
```
