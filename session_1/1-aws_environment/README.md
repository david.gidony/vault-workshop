# AWS Environment

## Overview

In order to practice this lab, first set some aws environment to authenticate to the aws account, then connect to the ec2 instance.

## 1. Setting environment variables

Environment variables are variables that define your environment. Define your own variables and save yourself time when writing scripts that contain APIs or executables.

### linux/mac

```
export AWS_ACCESS_KEY_ID=<AWS_ACCESS_KEY_ID> \
export AWS_SECRET_ACCESS_KEY=<AWS_SECRET_ACCESS_KEY> \
export AWS_DEFAULT_REGION=eu-central-1
```

Verify that your variables were set properly:

```
echo $AWS_ACCESS_KEY_ID
echo $AWS_SECRET_ACCESS_KEY
echo $AWS_DEFAULT_REGION
```

### powershell

```
$Env:AWS_ACCESS_KEY_ID=<”AWS_ACCESS_KEY_ID”>
$Env:AWS_SECRET_ACCESS_KEY=<”AWS_SECRET_ACCESS_KEY”>
$Env:AWS_DEFAULT_REGION=”eu-central-1”
```

Verify that your variables were set properly:

```
echo $Env:AWS_ACCESS_KEY_ID
echo $Env:AWS_SECRET_ACCESS_KEY
echo $Env:AWS_DEFAULT_REGION
```

## 2. SSH into your vm instance

```
chmod 400 mycert.pem
ssh -i "mycert.pem" ubuntu@<Instance External IP>
```

(Output. Don't copy!)
```
The authenticity of host '35.157.197.126 (35.157.197.126)' can't be established.
ECDSA key fingerprint is SHA256:qMd55+2dOq0YL29IxETGUnYx3Wy0aWPU0LbirMU9+SI.
Are you sure you want to continue connecting (yes/no/[fingerprint])?
```

Type "yes" to continue
