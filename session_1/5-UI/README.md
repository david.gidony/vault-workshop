# Vault UI

## Overview

Vault features a web-based user interface (UI) that enables you to unseal, authenticate, manage policies and secrets engines

## Connect to the UI

Launch a web browser, and enter http://< Instance External IP >:8200/ui in the address

## Explore the UI

  ![](img/vault-ui.png) 

At this point the vault server is uninitialized and sealed
