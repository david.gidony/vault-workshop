# AWS Environment

## Overview

In order to practice this lab, first get your ec2 server ip, then connect to the ec2 instance.

```
ssh -i "demo.pem" ubuntu@<Instance External IP>
```