# Prerequisites

## Overview

This lab requires Amazon CLI, Kubernetes CLI ,Helm CLI installed

### 1. Install Amazon CLI

1. Install unzip:

```
sudo apt install unzip
```

2. Download the awscliv2.zip file:

```
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" 
```

3. Extract the file:

```
unzip awscliv2.zip
```

4. Install:

```
sudo ./aws/install
```

5. verify the installation:

```
/usr/local/bin/aws --version
```

The output should be:

```
aws-cli/2.1.8 Python/3.7.3 Linux/5.4.0-1029-aws exe/x86_64.ubuntu.20 prompt/off
```

### 2. Install kubectl

1. Download the latest release with the command:

```
curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
```

2. Make the kubectl binary executable

```
chmod +x ./kubectl
```

3. Move the binary in to your PATH

```
sudo mv ./kubectl /usr/local/bin/kubectl
```

4. Test to ensure the version you installed is up-to-date

```
kubectl version –client
```

The output should be:

```
Client Version: version.Info{Major:"1", Minor:"20", GitVersion:"v1.20.0", GitCommit:"af46c47ce925f4c4ad5cc8d1fca46c7b77d13b38", GitTreeState:"clean", BuildDate:"2020-12-08T17:59:43Z", GoVersion:"go1.15.5", Compiler:"gc", Platform:"linux/amd64"}
```

5. To create or update the kubeconfig file for your lab cluster, run the following command: @@@@@@@##########$$$$$$$$$%%%%%%%

```
export AWS_ACCESS_KEY_ID=<AWS_ACCESS_KEY_ID>
export AWS_SECRET_ACCESS_KEY=<AWS_SECRET_ACCESS_KEY>
export AWS_DEFAULT_REGION=eu-central-1

aws eks --region eu-central-1 update-kubeconfig --name Vault-Lab
```

6. To test your configuration, run the following command:

```
kubectl get svc
```

The output for example should be:

```
NAME         TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
kubernetes   ClusterIP   172.20.0.1   <none>        443/TCP   45h
```

### 3. Install helm

Download and install

```
curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash
```

The output should be:

```
helm installed into /usr/local/bin/helm
```
