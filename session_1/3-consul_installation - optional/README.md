# Consul Installation

## Overview

Consul is a service mesh solution providing a full featured control plane with service discovery, configuration, and segmentation functionality. Each of these features can be used individually as needed, or they can be used together to build a full service mesh.
The key features of Consul are Service Discovery, Health Checking, Secure Service Communication and KV Store.
In our lab consul will be used as a KV backend storage for vault.

## Install Consul

1. Add the HashiCorp GPG key

```
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
```

2. Add the official HashiCorp Linux repository

```
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
```

3. Update and install

```
sudo apt-get update && sudo apt-get install consul
```

4. Verify the installation

```
consul
```

The output should be:

```
usage: consul [--version] [--help] <command> [<args>]

Available commands are:
    agent          Runs a Consul agent
    event          Fire a new event

...
```

## Consul server configuration

1. Create a configuration file at /etc/consul.d/server.hcl

```
sudo touch /etc/consul.d/server.hcl
```

```
sudo chown --recursive consul:consul /etc/consul.d
```

```
sudo chmod 640 /etc/consul.d/server.hcl
```

2. Add this configuration to the server.hcl configuration file

Copy this configuration:

```
# server.hcl
server = true
bootstrap_expect = 1
datacenter = "dc1"
data_dir = "/opt/consul"
ui = true
client_addr = "0.0.0.0"
bind_addr = "0.0.0.0"
log_level = "DEBUG"
```

Open text editor:

```
sudo vi /etc/consul.d/server.hcl
```

Paste the configuration and then :qw and hit ENTER to write and quit from the text editor

## Configure the consul systemd service

```
sudo touch /usr/lib/systemd/system/consul.service
```

## Start the Consul service

Enable and start Consul using the systemctl command responsible for controlling systemd managed services

```
sudo systemctl enable consul
```

```
sudo systemctl start consul
```

Check the status of the consul service using systemctl

```
sudo systemctl status consul
```

The output should be:

```
● consul.service - "HashiCorp Consul - A service mesh solution"
     Loaded: loaded (/lib/systemd/system/consul.service; enabled; vendor preset: enabled)
     Active: active (running) since Wed 2020-12-09 13:18:04 UTC; 8s ago
       Docs: https://www.consul.io/
   Main PID: 16804 (consul)
      Tasks: 8 (limit: 4642)
     Memory: 14.3M
     CGroup: /system.slice/consul.service
             └─16804 /usr/bin/consul agent -config-dir=/etc/consul.d/

Dec 09 13:18:11 ip-10-1-101-114 consul[16804]:     2020-12-09T13:18:11.061Z [DEBUG] agent.server: successfully established leadership: duration=2.440321ms
Dec 09 13:18:11 ip-10-1-101-114 consul[16804]:     2020-12-09T13:18:11.061Z [INFO]  agent.server: member joined, marking health alive: member=ip-10-1-101-114
Dec 09 13:18:11 ip-10-1-101-114 consul[16804]:     2020-12-09T13:18:11.061Z [DEBUG] agent.server.autopilot: autopilot is now running
Dec 09 13:18:11 ip-10-1-101-114 consul[16804]:     2020-12-09T13:18:11.061Z [DEBUG] agent.server.autopilot: state update routine is now running
Dec 09 13:18:11 ip-10-1-101-114 consul[16804]:     2020-12-09T13:18:11.070Z [INFO]  agent.server: federation state anti-entropy synced
Dec 09 13:18:11 ip-10-1-101-114 consul[16804]:     2020-12-09T13:18:11.130Z [DEBUG] agent: Skipping remote check since it is managed automatically: check=serfHealth
Dec 09 13:18:11 ip-10-1-101-114 consul[16804]:     2020-12-09T13:18:11.132Z [INFO]  agent: Synced node info
Dec 09 13:18:11 ip-10-1-101-114 consul[16804]:     2020-12-09T13:18:11.132Z [DEBUG] agent: Node info in sync
Dec 09 13:18:12 ip-10-1-101-114 consul[16804]:     2020-12-09T13:18:12.168Z [DEBUG] agent: Skipping remote check since it is managed automatically: check=serfHealth
Dec 09 13:18:12 ip-10-1-101-114 consul[16804]:     2020-12-09T13:18:12.169Z [DEBUG] agent: Node info in sync
```