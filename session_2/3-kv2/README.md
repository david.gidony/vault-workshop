# Versioned Key/Value Secrets Engine

## Overview

The KV secrets engine v1 do not provide a way to version or roll back secrets. This made it difficult to recover from unintentional data loss or overwrite when more than one user is writing at the same path,
The solution is version 2 of KV secrets engine which can retain a configurable number of secret versions. This enables older versions' data to be retrievable in case of unwanted deletion or updates of the data. In addition, its Check-and-Set operations can be used to protect the data from being overwritten unintentionally.

## Check the KV secrets engine version

Display all the enabled secrets engine

```
vault secrets list -detailed
```

The results display a table of all enabled secrets engines.

## Enable KV v2 secrets engine at secret/ path

```
vault secrets enable -path=secret kv-v2
```

The output should be:

```
Success! Enabled the kv-v2 secrets engine at: secret/
```

## Write secrets

Write a secret to the path secret/data/creds (We generate a token from a policy we created at the policy session)

```
VAULT_TOKEN=<token> vault kv put secret/creds password="my-long-password"
```

Be sure to replace < token > with the token value you copied (e.g. VAULT_TOKEN=s.X6gvFko7chPilgV0lpWXsdeu). This overwrites the VAULT_TOKEN value which is currently set to the initial root token

The output should be:

```
Key              Value
---              -----
created_time     2018-04-14T00:05:47.115378933Z
deletion_time    n/a
destroyed        false
version          1
```

The secret stores metadata along with the secret data. The version is an auto-incrementing number that starts at 1.

The policy only enables the read capability for the secret/data/foo path. An attempt to write to this path results in a "permission denied" error

Attempt to write to the secret/data/foo path

```
VAULT_TOKEN=<token> vault kv put secret/foo robot=beepboop
```

This policy defines a limited set of paths and capabilities. Without access to sys, commands like vault policy list or vault secrets list will not work.

Let's pass to generate token as environment variable:

```
VAULT_TOKEN=< Token >
```

Create secret at the same path secret/data/creds but with different secret data

```
vault kv put secret/creds password="my-very-long-password"
```

This secret data is replaced and the verion is incremented to 2.

```
Key              Value
---              -----
created_time     2018-04-14T00:13:35.296018431Z
deletion_time    n/a
destroyed        false
version          2
```

Get the secret defined at the path secret/data/creds with the root token as the generate token policy don't allow get.

```
VAULT_TOKEN=<Root Token> vault kv get secret/creds
```

the output:

```
====== Metadata ======
Key              Value
---              -----
created_time     2020-12-10T07:59:28.531194559Z
deletion_time    n/a
destroyed        false
version          2

====== Data ======
Key         Value
---         -----
password    my-very-long-password
```

The output displays the second secret. Creating a secret at the same path replaces the existing data; fields are not merged together. The secret data stored in earlier versions is still accessible but it is no longer returned by default.

## Retrieve a specific version of secret

You may run into a situation where you need to view the secret before an update.
Get version 1 of the secret defined at the path secret/data/creds.

```
VAULT_TOKEN=<Root Token> vault kv get -version=1 secret/creds
```

The output

```
====== Metadata ======
Key              Value
---              -----
created_time     2020-12-09T21:08:12.6537951Z
deletion_time    n/a
destroyed        false
version          1

====== Data ======
Key         Value
---         -----
password    my-long-password
```

Get the metadata of the secret defined at the path secret/data/creds.

```
VAULT_TOKEN=<Root Token> vault kv metadata get secret/creds
```

The output:

```
========== Metadata ==========
Key                     Value
---                     -----
cas_required            false
created_time            2020-12-09T21:08:12.6537951Z
current_version         3
delete_version_after    0s
max_versions            0
oldest_version          0
updated_time            2020-12-10T07:59:28.531194559Z

====== Version 1 ======
Key              Value
---              -----
created_time     2020-12-09T21:08:12.6537951Z
deletion_time    n/a
destroyed        false

====== Version 2 ======
Key              Value
---              -----
created_time     2020-12-09T21:10:40.694105038Z
deletion_time    n/a
destroyed        false
```

## Delete versions of secret

Delete version 2 of the secrets at path secret/data/creds.

```
VAULT_TOKEN=<Root Token> vault kv delete -versions="2" secret/creds
```

The output:

```
Success! Data deleted (if it existed) at: secret/creds
```

Get the metadata of the secret defined at the path secret/data/creds.

```
VAULT_TOKEN=<Root Token> vault kv metadata get secret/creds
```

The output:

```
========== Metadata ==========
Key                     Value
---                     -----
cas_required            false
created_time            2020-12-09T21:08:12.6537951Z
current_version         3
delete_version_after    0s
max_versions            0
oldest_version          0
updated_time            2020-12-10T07:59:28.531194559Z

====== Version 1 ======
Key              Value
---              -----
created_time     2020-12-09T21:08:12.6537951Z
deletion_time    n/a
destroyed        false

====== Version 2 ======
Key              Value
---              -----
created_time     2020-12-09T21:10:40.694105038Z
deletion_time    2020-12-10T08:19:29.599631236Z
destroyed        false
```

The metadata on versions 2 reports its deletion timestamp (deletion_time); however, the destroyed parameter is set to false.

We can undelete verion 2 of the secrets at path secret/data/creds.

```
VAULT_TOKEN=<Root Token> vault kv undelete -versions=2 secret/creds
```

## Permanently delete data

Destroy version 2 of the secrets at path secret/data/creds.

```
VAULT_TOKEN=<Root Token> vault kv destroy -versions=2 secret/creds
```

The output:

```
Success! Data written to: secret/destroy/creds
```

If we run the metadata command again we will see key destroyes with value true

Delete all versions of the secret at the path secret/data/creds.

```
VAULT_TOKEN=<Root Token> vault kv metadata delete secret/creds
```

The output:

```
Success! Data deleted (if it existed) at: secret/metadata/creds
```

## vault UI

Check the vault UI, We enabled the kv v2 secret engine

  ![](img/kv-v2.png)

  ![](img/kv-v2-password.png)  
