# Exercise - Vault UI

Lets practice what we have just learned about policies and kv version 2,
But this time we will perform everything from the UI.

1. Open a web browser and launch the Vault UI and then login.

2. Create a policy.

3. Write secret.

4. update the existing secret.

5. Retrieve a specific version of secret.

6. create a new version of the secrets based on an older version.

7. Specify the number of versions to keep (Tip: use the command shell, and type 
   vault write secret/config max_versions=4).

8. Overwrite the data a few more times to see what happens to its versions.

9. Delete versions of secret.

10. Undelete version of a secret.

11. permanently delete a version of secret.