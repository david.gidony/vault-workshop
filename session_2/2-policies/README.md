# Policies

## Overview

Policies in Vault control what a user can access.
For authentication Vault has multiple options or methods that can be enabled and used. Vault always uses the same format for both authorization and policies. All auth methods map identities back to the core policies that are configured with Vault.

## Policy Format

Policies are authored in HCL, but are JSON compatible. Here is an example policy:

```
path "secret/data/*" {
  capabilities = ["create", "update"]
}

path "secret/data/foo" {
  capabilities = ["read"]
}
```

With this policy, a user could write any secret to secret/data/, except to secret/data/foo, where only read access is allowed. Policies default to deny, so any access to an unspecified path is not allowed.

The policy format uses a prefix matching system on the API path to determine access control. The most specific defined policy is used, either an exact match or the longest-prefix glob match. Since everything in Vault must be accessed via the API, this gives strict control over every aspect of Vault, including enabling secrets engines, enabling auth methods, authenticating, as well as secret access.

There are some built-in policies that cannot be removed. For example, the root and default policies are required policies and cannot be deleted. The default policy provides a common set of permissions and is included on all tokens by default. The root policy gives a token super admin permissions, similar to a root user on a linux machine.

View the default policy

```
vault policy read default
```

## Write a Policy

To write a policy, use vault policy write command

Create the policy named my-policy with the contents from stdin:

```
vault policy write my-policy - << EOF
# need these paths to grant permissions for KV secrets engine:
path "secret/data/*" {
  capabilities = ["create", "update"]
}

path "secret/data/foo" {
  capabilities = ["read"]
}
EOF
```

Successful output:

```
Success! Uploaded policy: my-policy
```

List all the policies

```
vault policy list
```

The output should be:

```
default
my-policy
root
```

View the contents of the my-policy policy:

```
vault policy read my-policy
```

The output should be:

```
# need these paths to grant permissions for KV secrets engine:
path "secret/data/*" {
  capabilities = ["create", "update"]
}

path "secret/data/foo" {
  capabilities = ["read"]
}
```

## Attach the policy to token

The policy we created provides limited management of secrets defined for the KV-V2 secrets engine. Policies are attached to tokens that Vault generates directly or through its various auth methods

Create a token and add the my-policy policy:

```
vault token create -policy=my-policy
```

The output should be:

```
Key                  Value
---                  -----
token                s.X6gvFko7chPilgV0lpWXsdeu
token_accessor       kwgaMv7lz09a5oULHIT3UQ6z
token_duration       768h
token_renewable      true
token_policies       ["default" "my-policy"]
identity_policies    []
policies             ["default" "my-policy"]
```

Copy the generated token value we will use it after we learn the KV secret engine.

The policy enables the create and update capabilties for every path within the secret/ engine except one,
the path secret/data/creds

## Wild Cards (* and +)

1. The glob (*) is a wildcard and can only be used at the end of a
   path

2. Can be used to signify anything “after” a path or as part of a
   pattern:  
   – secret/apps/application1/* - allows anything below application1
   – secret/path/db-* - would match secret/path/db-2 but not secret/path/db2

3. The plus (+) denotes any number of characters within a single
   path segment (secret/+/db - matches secret/db2/db or secret/app/db)  

4. Can be used in multiple path segments (i.e., secret/+/+/db)

## Using Deny

We can use the deny capability in order to restrict access to a specific location, for example:

```
path “secret/*” {
  capabilities = [“read”, “update”]
}
path “secret/apps/app1/super-secret” {
  capabilities = [“deny”]
}
```

## Detailed Policy

We can use detailed policy to allow a specific path or value

```
path “secret/apps/app1/*” {
  capabilities = [“read”, “update”]
  allowed_parameters = {
    “secret” = []
  }
}
```

This policy will allow read and update capabilities to a secret under secret/apps/app1/*
but only on secret path

## Administrative Policies

Hashicorp highly recommend to revoke the root token and use instead admin token with the appropriate policy
an example for admin policy:

```
# Configure License
path "sys/license" {
  capabilities = ["read", "list", "create", "update", "delete"]
}
# Initialize Vault
path "sys/init" {
  capabilities = ["read", "update", "create"]
}
# Configure UI in Vault
path "sys/config/ui" {
  capabilities = ["read", "list", "update", "delete", "sudo"]
}
# Allow rekey of unseal keys for Vault
path "sys/rekey/*" {
  capabilities = ["read", "list", "update", "delete"]
}
# Allows rotation of master key
path "sys/rotate" {
  capabilities = ["update", "sudo"]
}
# Allows Vault seal
path "sys/seal" {
  capabilities = ["sudo"]
}
```

## Policies for the UI

If you’re using the UI, you’ll likely need to add additional LIST permissions,
LIST doesn’t allow the user to READ secrets,
Without LIST, the user cannot browse to the desired path/secret,
The lack of LIST permissions is sort of security by obscurity
Example policy for the UI:

```
path “secret/apps/app1/secret” {
  capabilities = [“read”, “update”]
}
path “secret/*” {
  capabilities = [“list”]
}
```