# Vault Workshop

Lab to practice hashicorp vault.
Vault is a tool to secure, store and tightly control access to tokens, passwords, certificates, encryption keys for protecting secrets and other sensitive data using a UI, CLI, or HTTP API.

## Prerequisites and installations
1. **Connecting to the lab** - [SSH](session_1/0-ssh)
2. **Hashicorp vault installation** - [Vault](session_1/4-vault_installation)
3. **Intruducing vault UI** - [UI](session_1/5-UI)

## Vault
1. **Initialize and Unseal Vault** - [Initialize and Unseal](session_2/1-init_unseal) 
2. **Policies** - [Policies](session_2/2-policies)
3. **Secret Engine KV version 2** - [KV2](session_2/3-kv2)
4. **Exercise: Policies and KV version 2 from the UI** - [Exercise](session_2/4-exercise)
5. **Solution: Walkthrough** - [Solution](session_2/5-solution)
6. **Secret Engine: MYSQL Database (dynamic credentials)** - [Dynamic Credentials](session_2/6-mysql_dynamic_credentials)
7. **Auth Methods: Kubernetes** - [Auth Methods - Kubernetes](session_2/7-auth_methods_kubernetes)
8. **Audit Devices** - [Audit Devices - File](session_2/8-audit_devices)