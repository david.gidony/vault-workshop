output "instance_ip_addr" {
  value = aws_instance.vaultDemo.*.public_ip
}
output "public_dns" {
  value = aws_instance.vaultDemo.*.public_dns
}