provider "aws" {
  region     = "eu-west-2"
  access_key = ""
  secret_key = ""
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "vaultDemo" {
  count = 15
  key_name = "dude-demo"
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
 # iam_instance_profile = "arn:aws:iam::391026464511:instance-profile/Harel-vault-demo"
  subnet_id = "subnet-842699c8"
  vpc_security_group_ids = ["sg-0dd5094ac97fc9d96"]
  root_block_device {
    delete_on_termination = false
    volume_size = 10
    volume_type = "gp2"
  }
  tags = {
    Name = "vaultdemo-${count.index}"
    CreatedBy = "David Gidony"
    Purpose = "Workshop"
  }
user_data = <<-EOF
#! /bin/bash
sudo apt-get update
sudo apt-get install -y unzip
sudo apt install -y mysql-client-core-8.0
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl
#aws eks --region eu-west-2 update-kubeconfig --name vault-demo-cluster
echo "vault-demo-db.cceqegpvmgyj.eu-west-2.rds.amazonaws.com" > /home/ubuntu/db-srv.txt
EOF
}